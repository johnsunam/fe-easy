import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginSignupComponent } from './login-signup/login-signup.component';
import { ProfileComponent } from './profile/profile.component';
import { LayoutComponent } from './layout/layout.component';
import { HeaderComponent } from './header/header.component';
import { ResetComponent } from './forgotPassword/reset.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AlertService, AuthenticationService, UserService } from './_services';
import { AuthGuard } from './_guards';
import { AlertComponent } from './_directives';
import { AcademicComponent } from './academic/academic.component';
import { JwtInterceptor } from './_helpers/jwt.interceptors';

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    LoginSignupComponent,
    ProfileComponent,
    LayoutComponent,
    HeaderComponent,
    AcademicComponent,
    ResetComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,

  ],
  providers: [
    AlertService,
    AuthenticationService,
    AuthGuard,
    UserService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  ],
  bootstrap: [  AppComponent  ]
})
export class AppModule { }
