import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from '../custom-validators';
import { AuthenticationService, AlertService, UserService } from '../_services';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
declare var $:any;
@Component({
  selector: 'app-login-signup',
  templateUrl: './login-signup.component.html',
  styleUrls: ['./login-signup.component.css']
})


export class LoginSignupComponent implements OnInit {
    @ViewChild('generateOtp') generateOtp: ElementRef;
    @ViewChild('closeVerify') closeVerify: ElementRef;
    @ViewChild('signup') signup: ElementRef;
    @ViewChild('signin') signin: ElementRef;


    loginForm: FormGroup;
    signupForm: FormGroup;
    forgotPasswordForm: FormGroup;
    submitted = false;
    register = false;
    forgotPassword = false;
    otpVerified = false;
    otpNum: string;
    otpError = null;

  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private userService: UserService,
    private router: Router
    ) { }

  dropdown: string = null;

  ngOnInit() {
    $('#exampleModal').on('hidden.bs.modal', function () {

      console.log('toggle dropdown')
      //$('#sidebar_filter_areas').trigger('click.bs.dropdown');

      $('#signUp').dropdown('toggle');;
     })
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required
      // Validators.compose([
      //   Validators.required,
      //    // check whether the entered password has a number
      //    CustomValidators.patternValidator(/\d/, {
      //     hasNumber: true
      //   }),
      //   // check whether the entered password has upper case letter
      //   CustomValidators.patternValidator(/[A-Z]/, {
      //     hasCapitalCase: true
      //   }),
      //   // check whether the entered password has a lower case letter
      //   CustomValidators.patternValidator(/[a-z]/, {
      //     hasSmallCase: true
      //   }),
      //   // check whether the entered password has a special character
      //   CustomValidators.patternValidator(
      //     /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
      //     {
      //       hasSpecialCharacters: true
      //     }
      //   ),
      //   Validators.minLength(8)
      // ])
    ]
  });

  this.forgotPasswordForm = this.formBuilder.group({
    emailId: ['', [Validators.email]],
    mobile: [],
    type: []
  });

  this.signupForm = this.formBuilder.group({
    firstName: ['', [Validators.required]],
    lastName: [],
    email: ['', [Validators.required, Validators.email]],
    password: ['', Validators.required
    // Validators.compose([
    //   Validators.required,
    //    // check whether the entered password has a number
    //    CustomValidators.patternValidator(/\d/, {
    //     hasNumber: true
    //   }),
    //   // check whether the entered password has upper case letter
    //   CustomValidators.patternValidator(/[A-Z]/, {
    //     hasCapitalCase: true
    //   }),
    //   // check whether the entered password has a lower case letter
    //   CustomValidators.patternValidator(/[a-z]/, {
    //     hasSmallCase: true
    //   }),
    //   // check whether the entered password has a special character
    //   CustomValidators.patternValidator(
    //     /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
    //     {
    //       hasSpecialCharacters: true
    //     }
    //   ),
    //   Validators.minLength(8)
    // ])
  ],
    phone: ['', [Validators.required]]
  });

  }

  get l() { return this.loginForm.controls; }
  get r() { return this.signupForm.controls; }
  get f() { return this.forgotPasswordForm.controls; }

  clickEvent (val) {
    this.dropdown = val;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    console.log(this.loginForm.controls.password.value);
    if (this.loginForm.invalid) {
        return;
    }

    this.authenticationService.login(this.loginForm.controls.email.value, this.loginForm.controls.password.value )
    .pipe(first())
    .subscribe(
      data => {
            this.router.navigate(['/profile']);
      },
      error => {
        this.alertService.error(error.error.error_description);
      }
    );
  }

  sendOtp () {
    // console.log('otp verification')
    if (this.signupForm.controls.phone.value) {

      this.userService.sendOtp({phone: this.signupForm.controls.phone.value })
      .pipe(first())
      .subscribe(
        data => {
          if (data.messageType === 'success') {
              let el: HTMLElement = this.generateOtp.nativeElement as HTMLElement;
              el.click();
            // this.alertService.error(data.message)
          }
        },
        error => {
          this.alertService.error(error);
        }
      )
    }
  }

  verifyOtp () {
    if (this.otpNum) {
      this.userService.verifyOtp({phone: this.signupForm.controls.phone.value, otp: this.otpNum})
      .pipe(first())
      .subscribe(
        data => {
          if(data.messageType == 'error') {
            // let el: HTMLElement = this.closeVerify.nativeElement as HTMLElement;
            // el.click();
            // this.otpVerified = true;
            this.otpError = data.message
          } else {
            let el: HTMLElement = this.closeVerify.nativeElement as HTMLElement;
            el.click();
            this.otpVerified = true;
          }
        },
        error => {
          console.log('errrrr', error)
        }
      )
    }
  }

  onSignup () {
    this.register = true;

    if (this.signupForm.invalid) {
      console.log(this.signupForm.controls);
      return;
    }
    this.userService.register( this.signupForm.value)
      .pipe(first())
      .subscribe(
        data => {
          if (data.messageType === 'Success') {
            let el: HTMLElement = this.signup.nativeElement as HTMLElement;
            el.click();
            this.alertService.success(data.message, true);
            this.otpVerified = true;
            this.signupForm.reset();
          } else {
            this.alertService.error(data.message, true);
          }
        },
        error => {
          console.log('errrrorrrrrr', error);
          this.alertService.error(error);
        });

  }

  onForgotPasssword () {
    this.forgotPassword = true;

    if (this.forgotPasswordForm.invalid) {
      console.log('invalid',this.forgotPasswordForm);
      return;
    }
    this.userService.forgotPassword(this.forgotPasswordForm.value)
    .pipe(first())
    .subscribe(
      data => {
      console.log('forgot password response', data);
      this.forgotPasswordForm.reset();
      let el: HTMLElement = this.signin.nativeElement as HTMLElement;
      el.click();
      //   if (data) {
      //   this.alertService.success('Otp sent to the registered Email of the user', true);
      // } else {
      //   this.alertService.error('Given email is not a registered emailId', true);
      // }
      },
      error => {
        this.alertService.error(error);
      }
    );
  }
}
