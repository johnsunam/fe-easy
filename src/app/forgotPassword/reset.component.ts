import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from '../custom-validators';
import { AuthenticationService, AlertService, UserService } from '../_services';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';


@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html'
  // styleUrls: ['./app.component.css']
})
export class ResetComponent implements OnInit {

  resetForm: FormGroup;
  token = null;
  reset = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private userService: UserService,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private router: Router
  ) { }
  ngOnInit() {
    this.resetForm = this.formBuilder.group({
      password: ['', Validators.compose([
        Validators.required,
         // check whether the entered password has a number
         CustomValidators.patternValidator(/\d/, {
          hasNumber: true
        }),
        // check whether the entered password has upper case letter
        CustomValidators.patternValidator(/[A-Z]/, {
          hasCapitalCase: true
        }),
        // check whether the entered password has a lower case letter
        CustomValidators.patternValidator(/[a-z]/, {
          hasSmallCase: true
        }),
        // check whether the entered password has a special character
        CustomValidators.patternValidator(
          /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
          {
            hasSpecialCharacters: true
          }
        ),
        Validators.minLength(8)
      ])],
  });
    console.log('token =====', this.token);
    this.route.params.subscribe(params => { this.token = params['token']; });
    console.log(this.token)
  }

  onSubmit() {
    console.log('token ', this.token)
    this.reset = true;
    if (this.resetForm.invalid) {
      return;
    }
    this.userService.resetPassword({newPassword: this.resetForm.controls.password.value, token: this.token})
      .pipe(first())
      .subscribe(
        data => {
          if (data.messageType === 'success') {
            // this.alertService.success(data.message)
            this.router.navigate(['/'])
          }else {
            this.alertService.error(data.message)
          }
        },
        error => {
          this.alertService.error(error);
        }
      )



  }

  get r() { return this.resetForm.controls; }

}
