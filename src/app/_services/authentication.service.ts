import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

// import { environment } from '../../environments/environment';



@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient) { }

    login(username: string, password: string) {
      // const apiUrl = 'https://fees.us-east-1.elasticbeanstalk.com/turiya/';
      // username = 'feeasy.admin@turiyasoftech.com';
      // password = 'admin@123';
        return this.http.post<any>(` https://devapiv2.feeasy.co.in/oauth/token?grant_type=password&scope=read write&client_id=feeasyUI&client_secret=topSecret&username=${username}&password=${password}`,{})
            .pipe(map(res => {
              console.log('======', res);
              if (res.error) {
                return false;
              }
              localStorage.setItem('currentUser', JSON.stringify(res));
              return true;
                // login successful if there's a jwt token in the response
                // if (user && user.token) {
                //     // store user details and jwt token in local storage to keep user logged in between page refreshes
                //     localStorage.setItem('currentUser', JSON.stringify(user));
                // }

            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
}
