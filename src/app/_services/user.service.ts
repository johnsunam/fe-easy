import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { User } from '../_models';

const apiUrl = 'https://devapiv2.feeasy.co.in/user/';

@Injectable()
export class UserService {
  constructor(
    private http: HttpClient,
    ) { }

  register(user: User) {
    const data = {role: 'ROLE_USER',emailId: user.email, firstname: user.firstName, lastname: user.lastName, password: user.password, phoneNumber: user.phone };
    return this.http.post<any>(`${apiUrl}register`, data)
      .pipe(map(res => {
        console.log('dddddddd', res);
          // if (res.messageType === 'Success') {
            return res;
          // } else {
          //   return ;
          // }
      }));
  }

  resetPassword(data) {
    return this.http.post<any>('https://devapiv2.feeasy.co.in/pwdrecovery/changePassword',data)
    .pipe(map(res => {
      return res;
    }))
  }



  verifyOtp (data) {
    console.log('verify', data);
    return this.http.post<any>(apiUrl + 'verifyOtp', {mobileNumber: data.mobileNum, otp: data.otp})
    .pipe(map(res => {
      console.log('resss verify', res)
      return res;
    }));
  }

  sendOtp (data) {
    console.log('send', data);
    return this.http.post<any>(apiUrl + 'sendOtp', {mobileNumber: data.phone})
    .pipe(map(res => {
      return res;
    }));
  }

  forgotPassword(data) {
    console.log('forgot password', data)
    const api = data.type === 'mob' ? `phoneNumber/${data.mobile}` : `/pwdrecovery/${data.emailId}`;
    return this.http.get<any>(`${'https://devapiv2.feeasy.co.in'}${api}`)
    .pipe(map(res => {
      return res;
      // if (res.result === 'SUCCESS') {
      //   return true;
      // } else {
      //   return false;
      // }
    }));
  }

  changePassword(data) {
    const apiUrl = 'https://devapiv2.feeasy.co.in/pwdrecovery/changePassword';
    return this.http.post<any>(apiUrl, data)
    .pipe(map(res => {
      return res;
    }));
  }

}
