import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginSignupComponent } from '../app/login-signup/login-signup.component';
import { ProfileComponent } from '../app/profile/profile.component';
import { LayoutComponent } from '../app/layout/layout.component';
import { ResetComponent } from './forgotPassword/reset.component';
import { AuthGuard } from './_guards';


const appRoutes = [
  { path: '', component: LoginSignupComponent },
  { path: 'reset/:token', component: ResetComponent},
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: 'profile', component: ProfileComponent, pathMatch: 'full'}
    ],
    canActivate: [AuthGuard]
  }

];
@NgModule({
  exports: [ RouterModule ],
  imports: [
    // CommonModule,
    RouterModule.forRoot(
appRoutes,
      {enableTracing: true})
  ],
  declarations: []
})
export class AppRoutingModule { }
