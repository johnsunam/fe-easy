import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from '../custom-validators';
import { Router } from '@angular/router';
import { AuthenticationService, AlertService, UserService } from '../_services';
import { first } from 'rxjs/operators';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @ViewChild('profile_drop') profile_drop: ElementRef;

  changePasswordForm: FormGroup;
  confirmMatch = true;
  submitted = false;
  otpForm: FormGroup;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private userService: UserService
  ) { }
  changePassword: Boolean = false;
  dropDown: Boolean = false;
  opt: Boolean = false;
  otpType: String = '';

  ngOnInit() {
    this.changePasswordForm = this.formBuilder.group({
      new_pwd: ['', Validators.compose([
        Validators.required,
         // check whether the entered password has a number
         CustomValidators.patternValidator(/\d/, {
          hasNumber: true
        }),
        // check whether the entered password has upper case letter
        CustomValidators.patternValidator(/[A-Z]/, {
          hasCapitalCase: true
        }),
        // check whether the entered password has a lower case letter
        CustomValidators.patternValidator(/[a-z]/, {
          hasSmallCase: true
        }),
        // check whether the entered password has a special character
        CustomValidators.patternValidator(
          /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
          {
            hasSpecialCharacters: true
          }
        ),
        Validators.minLength(8)
      ])],
      // confirm_pwd: ['', Validators.compose([
      //   Validators.required,
      //    // check whether the entered password has a number
      //    CustomValidators.patternValidator(/\d/, {
      //     hasNumber: true
      //   }),
      //   // check whether the entered password has upper case letter
      //   CustomValidators.patternValidator(/[A-Z]/, {
      //     hasCapitalCase: true
      //   }),
      //   // check whether the entered password has a lower case letter
      //   CustomValidators.patternValidator(/[a-z]/, {
      //     hasSmallCase: true
      //   }),
      //   // check whether the entered password has a special character
      //   CustomValidators.patternValidator(
      //     /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
      //     {
      //       hasSpecialCharacters: true
      //     }
      //   ),
      //   Validators.minLength(8)
      // ])]
      confirm_pwd: []
    });

    this.otpForm = this.formBuilder.group({
        mobile: [],
        otp: [],
    });
  }

  get p() { return this.changePasswordForm.controls; }

  clickEvent () {
    this.changePassword = !this.changePassword;
  }

  clickOpt () {
    this.opt = !this.opt;
  }

  showDropdown () {
    this.dropDown = !this.dropDown;
    console.log('......///', this.dropDown);
  }

  logout () {
    localStorage.clear();
    this.router.navigate(['/']);
  }

  onSubmit () {
    this.submitted = true;
    if (this.changePasswordForm.controls.new_pwd.value === this.changePasswordForm.controls.confirm_pwd.value) {
      this.confirmMatch = true;
      if (this.changePasswordForm.invalid) {
        return;
      } else {
        const userDetail = JSON.parse(localStorage.getItem('currentUser'));
        console.log('userDetail========', userDetail);
        this.userService.changePassword({newPassword: this.changePasswordForm.controls.new_pwd.value, token: userDetail.access_token })
        .pipe(first())
      .subscribe(
        data => {
          if (data.messageType === 'error') {
            this.alertService.error(data.message);
          } else {
            let el: HTMLElement = this.profile_drop.nativeElement as HTMLElement;
            el.click();
            this.changePassword = false;
            this.alertService.success(data.message);
          }

        },
        error => {
          console.log('errrrorrrrrr', error);
          this.alertService.error(error);
        });
      }
    } else {
      this.confirmMatch = false;

      this.alertService.error('confirm and new password not match.', true);
    }
    console.log(this.changePasswordForm.controls);
  }

  otpChange (type) {
    this.otpType = type;
  }

  onSubmitOtp () {
    console.log(this.otpForm.controls.mobile);
    if (this.otpType === 'verify') {
      this.userService.verifyOtp({mobileNum: this.otpForm.controls.mobile.value, otp: this.otpForm.controls.otp.value})
      .pipe(first())
      .subscribe(data => {
          console.log(data);
          if (data.messageType === 'Success') {
            this.alertService.success(data.message);
            this.otpType = '';
          } else {
            this.alertService.error(data.message);
          }
        },
        error => {
          console.log('eeeee======', error.message);
          this.alertService.error(error.message);

        }
        );
    } else {
      this.userService.sendOtp({mobileNum: this.otpForm.controls.mobile.value})
      .pipe(first())
      .subscribe(data => {
        if (data.messageType === 'info') {
          this.alertService.success(data.message);
          this.otpType = '';
        } else {
          this.alertService.error('Error occured !!!');

        }
        },
        error => {
          console.log('eeeee======', error.message);
          this.alertService.error(error.message);

        });
    }


  }

}
