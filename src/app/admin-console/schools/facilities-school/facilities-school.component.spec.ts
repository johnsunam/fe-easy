import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilitiesSchoolComponent } from './facilities-school.component';

describe('FacilitiesSchoolComponent', () => {
  let component: FacilitiesSchoolComponent;
  let fixture: ComponentFixture<FacilitiesSchoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilitiesSchoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilitiesSchoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
