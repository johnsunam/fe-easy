import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitutionSchoolComponent } from './institution-school.component';

describe('InstitutionSchoolComponent', () => {
  let component: InstitutionSchoolComponent;
  let fixture: ComponentFixture<InstitutionSchoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstitutionSchoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitutionSchoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
