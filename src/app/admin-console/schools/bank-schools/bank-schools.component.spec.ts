import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankSchoolsComponent } from './bank-schools.component';

describe('BankSchoolsComponent', () => {
  let component: BankSchoolsComponent;
  let fixture: ComponentFixture<BankSchoolsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankSchoolsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankSchoolsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
