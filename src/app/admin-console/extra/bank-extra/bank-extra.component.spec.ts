import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankExtraComponent } from './bank-extra.component';

describe('BankExtraComponent', () => {
  let component: BankExtraComponent;
  let fixture: ComponentFixture<BankExtraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankExtraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankExtraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
