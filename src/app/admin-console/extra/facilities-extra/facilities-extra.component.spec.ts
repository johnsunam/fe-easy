import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilitiesExtraComponent } from './facilities-extra.component';

describe('FacilitiesExtraComponent', () => {
  let component: FacilitiesExtraComponent;
  let fixture: ComponentFixture<FacilitiesExtraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilitiesExtraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilitiesExtraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
