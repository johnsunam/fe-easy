import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitutionExtraComponent } from './institution-extra.component';

describe('InstitutionExtraComponent', () => {
  let component: InstitutionExtraComponent;
  let fixture: ComponentFixture<InstitutionExtraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstitutionExtraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitutionExtraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
