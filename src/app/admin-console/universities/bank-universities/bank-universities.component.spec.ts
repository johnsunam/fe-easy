import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankUniversitiesComponent } from './bank-universities.component';

describe('BankUniversitiesComponent', () => {
  let component: BankUniversitiesComponent;
  let fixture: ComponentFixture<BankUniversitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankUniversitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankUniversitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
