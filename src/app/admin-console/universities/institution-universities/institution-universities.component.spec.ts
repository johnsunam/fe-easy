import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitutionUniversitiesComponent } from './institution-universities.component';

describe('InstitutionUniversitiesComponent', () => {
  let component: InstitutionUniversitiesComponent;
  let fixture: ComponentFixture<InstitutionUniversitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstitutionUniversitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitutionUniversitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
