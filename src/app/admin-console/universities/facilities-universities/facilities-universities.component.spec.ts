import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilitiesUniversitiesComponent } from './facilities-universities.component';

describe('FacilitiesUniversitiesComponent', () => {
  let component: FacilitiesUniversitiesComponent;
  let fixture: ComponentFixture<FacilitiesUniversitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilitiesUniversitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilitiesUniversitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
