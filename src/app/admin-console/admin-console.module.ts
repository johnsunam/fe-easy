import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { AdminConsoleComponent } from './admin/admin-console.component';
import { InstitutionComponent } from './college/institution/institution.component';
import { FacilitiesComponent } from './college/facilities/facilities.component';
import { BankComponent } from './college/bank/bank.component';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { FacilitiesPlaySchoolComponent } from './play-school/facilities-play-school/facilities-play-school.component';
import { InstitutionPlaySchoolComponent } from './play-school/institution-play-school/institution-play-school.component';
import { BankSchoolsComponent } from './schools/bank-schools/bank-schools.component';
import { FacilitiesSchoolComponent } from './schools/facilities-school/facilities-school.component';
import { InstitutionSchoolComponent } from './schools/institution-school/institution-school.component';
import { InstitutionSportComponent } from './sport-academy/institution-sport/institution-sport.component';
import { FacilitiesSportComponent } from './sport-academy/facilities-sport/facilities-sport.component';
import { BankSportComponent } from './sport-academy/bank-sport/bank-sport.component';
import { InstitutionUniversitiesComponent } from './universities/institution-universities/institution-universities.component';
import { FacilitiesUniversitiesComponent } from './universities/facilities-universities/facilities-universities.component';
import { BankUniversitiesComponent } from './universities/bank-universities/bank-universities.component';
import { BankPlaySchoolComponent } from './play-school/bank-play-school/bank-play-school.component';
import { AdminSidebarComponent } from './admin-sidebar/admin-sidebar.component';
import { AdminFooterComponent } from './admin-footer/admin-footer.component';
import { BankExtraComponent } from './extra/bank-extra/bank-extra.component';
import { FacilitiesExtraComponent } from './extra/facilities-extra/facilities-extra.component';
import { InstitutionExtraComponent } from './extra/institution-extra/institution-extra.component';
import { ParentComponent } from './main/parent/parent.component';
import { PoliciesComponent } from './main/policies/policies.component';
import { ConfigurationComponent } from './main/configuration/configuration.component';
import { AdminBankComponent } from './main/admin-bank/admin-bank.component';
import { RolesComponent } from './main/roles/roles.component';

const appRoutes = [
  { path: 'college/institution', component: InstitutionComponent },
  { path: 'college/facilities', component: FacilitiesComponent },
  { path: 'college/bank', component: BankComponent },
  { path: 'school/institution', component: InstitutionSchoolComponent },
  { path: 'school/facilities', component: FacilitiesSchoolComponent },
  { path: 'school/bank', component: BankSchoolsComponent },
  { path: 'play-school/institution', component: InstitutionPlaySchoolComponent },
  { path: 'play-school/facilities', component: FacilitiesPlaySchoolComponent },
  { path: 'play-school/bank', component: BankPlaySchoolComponent },
  { path: 'sport-academy/institution', component: InstitutionSportComponent },
  { path: 'sport-academy/facilities', component: FacilitiesSportComponent },
  { path: 'sport-academy/bank', component: BankSportComponent },
  { path: 'universities/institution', component: InstitutionUniversitiesComponent },
  { path: 'universities/facilities', component: FacilitiesUniversitiesComponent },
  { path: 'universities/bank', component: BankUniversitiesComponent },
  { path: 'extra/institution', component: InstitutionExtraComponent },
  { path: 'extra/facilities', component: FacilitiesExtraComponent },
  { path: 'extra/bank', component: BankExtraComponent },
  { path: 'admin-bank', component: AdminBankComponent },
  { path: 'parent', component: ParentComponent },
  { path: 'configuration', component: ConfigurationComponent },
  { path: 'policies', component: PoliciesComponent },
  { path: 'roles', component: RolesComponent },
  { path: '', component: AdminConsoleComponent }
  
]


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      appRoutes)

      ],
  declarations: [
    AdminConsoleComponent,
    AdminHeaderComponent,
    InstitutionComponent,
    FacilitiesComponent,
    FacilitiesComponent,
    BankComponent, 
    FacilitiesPlaySchoolComponent,
    BankPlaySchoolComponent,
    InstitutionPlaySchoolComponent,
    BankSchoolsComponent,
    FacilitiesSchoolComponent, 
    InstitutionSchoolComponent,
    InstitutionSportComponent, 
    FacilitiesSportComponent, 
    BankSportComponent, 
    InstitutionUniversitiesComponent, 
    FacilitiesUniversitiesComponent,
    BankUniversitiesComponent,
    AdminSidebarComponent,
    AdminFooterComponent,
    BankExtraComponent,
    FacilitiesExtraComponent,
    InstitutionExtraComponent,
    ParentComponent,
    PoliciesComponent,
    ConfigurationComponent,
    AdminBankComponent,
    RolesComponent
  ]
})
export class AdminConsoleModule { }
