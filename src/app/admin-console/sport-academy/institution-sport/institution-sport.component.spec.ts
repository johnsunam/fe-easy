import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitutionSportComponent } from './institution-sport.component';

describe('InstitutionSportComponent', () => {
  let component: InstitutionSportComponent;
  let fixture: ComponentFixture<InstitutionSportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstitutionSportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitutionSportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
