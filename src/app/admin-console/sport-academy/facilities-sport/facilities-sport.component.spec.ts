import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilitiesSportComponent } from './facilities-sport.component';

describe('FacilitiesSportComponent', () => {
  let component: FacilitiesSportComponent;
  let fixture: ComponentFixture<FacilitiesSportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilitiesSportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilitiesSportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
