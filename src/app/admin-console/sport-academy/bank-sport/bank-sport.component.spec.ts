import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankSportComponent } from './bank-sport.component';

describe('BankSportComponent', () => {
  let component: BankSportComponent;
  let fixture: ComponentFixture<BankSportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankSportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankSportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
