import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankPlaySchoolComponent } from './bank-play-school.component';

describe('BankPlaySchoolComponent', () => {
  let component: BankPlaySchoolComponent;
  let fixture: ComponentFixture<BankPlaySchoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankPlaySchoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankPlaySchoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
