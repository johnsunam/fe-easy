import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacilitiesPlaySchoolComponent } from './facilities-play-school.component';

describe('FacilitiesPlaySchoolComponent', () => {
  let component: FacilitiesPlaySchoolComponent;
  let fixture: ComponentFixture<FacilitiesPlaySchoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacilitiesPlaySchoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacilitiesPlaySchoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
