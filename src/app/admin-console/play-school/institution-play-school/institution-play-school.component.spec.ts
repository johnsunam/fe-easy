import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitutionPlaySchoolComponent } from './institution-play-school.component';

describe('InstitutionPlaySchoolComponent', () => {
  let component: InstitutionPlaySchoolComponent;
  let fixture: ComponentFixture<InstitutionPlaySchoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstitutionPlaySchoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitutionPlaySchoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
