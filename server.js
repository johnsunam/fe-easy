const express = require('express');
const path = require('path');


const app = express();

app.use(express.static(__dirname+'/dist/fe-easy'));

app.get('*', function ( req, res ) {
  res.sendFile(path.join(__dirname+ '/dist/fe-easy/index.html'));

});

app.listen( process.env.PORT || 8000 );
